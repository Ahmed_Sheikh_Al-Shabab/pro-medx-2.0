<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\ImageTrait;

class Video extends Model
{
    use HasFactory;

    protected $with = ['videoUrls'];

    public function chapter(){
        return $this->belongsTo(Chapter::class);
    }

    public function videoUrls(){
        return $this->hasMany(VideoUrl::class);
    }
}
