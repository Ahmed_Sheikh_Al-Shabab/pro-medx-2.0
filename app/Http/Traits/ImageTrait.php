<?php
namespace App\Http\Traits;

use Illuminate\Support\Facades\Storage;

trait ImageTrait{
    public $baseUrl = 'http://pro-medx.com';
    public function decodeImageUrl($data){
        foreach ($data as $datum){
            if(empty(json_decode($datum['image']))) continue;
            $decodeUrl = Storage::url((json_decode($datum['image']))[0]->download_link);
            $fullUrl = $this->baseUrl.$decodeUrl;
            $datum['image'] = $fullUrl;
        }
        return $data;
    }
}
