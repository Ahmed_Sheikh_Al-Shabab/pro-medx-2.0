<?php

namespace App\Http\Controllers;

use App\Models\Qrcode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use SimpleSoftwareIO\QrCode\Facades\QrCode as QrCodeGenerator;
use TCG\Voyager\Facades\Voyager;

class QrcodeController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        $qrcode_name = Qrcode::all()->count();
        $this->generateQrcode($request->cash, $request->number_of_codes, $qrcode_name);
        $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        return $redirect->with([
            'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);
    }

    public function generateQrcode($cash, $number_of_codes, $qrcode_name){
        for($i=1; $i<=$number_of_codes; $i++){
            $qrcode_name++;
            $uuid = Uuid::uuid1();
            $qrcode = QrCodeGenerator::size('300')->generate($uuid->toString());
            Storage::disk('local')->put('/public/qrcodes/'.date("mdYHis").'_'.$qrcode_name.'.svg', $qrcode);
            $this->createQrcode($cash, $uuid, $qrcode_name);
        }
        return;
    }

    public function createQrcode($cash, $code, $qrcode_number){
        return Qrcode::create([
            'cash' => $cash,
            'code' => $code,
            'is_used' => 0,
            'qrcode_number' => $qrcode_number,
        ]);
    }

}
