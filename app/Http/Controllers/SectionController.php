<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\SubjectInstructor;
use Illuminate\Http\Request;
use App\Http\Traits\ImageTrait;

class SectionController extends Controller
{
    use ImageTrait;
    
    public function getSection(Request $request, $subject_instructor_id){
        $subject_instructor = SubjectInstructor::findOrFail($subject_instructor_id);
        $sections = $subject_instructor->sections()->get();
        $sections = $this->checkUserSections($request, $sections);
        $sections = $this->decodeImageUrl($sections);
        return response()->json([
            'status' => 'success',
            'message' => "Sections for subject instructor {$subject_instructor_id} retrieved successfully",
            'data' => $sections
        ]);
    }

    public function checkUserSections(Request $request, $sections){
        $user = $request->user();
        $user_sections = $user->sections()->get();
        foreach ($sections as $section){
            if($user_sections->contains($section->id)){
                $section->is_purchased = 1;
            }
        }
        return $sections;
    }
}
