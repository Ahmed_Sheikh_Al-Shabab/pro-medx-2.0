<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Traits\ImageTrait;

class InstructorController extends Controller
{
    use ImageTrait;
    
    public function getInstructor(Request $request, $subject_id){
        $subject = Subject::findOrFail($subject_id);
        $instructors = $subject->instructors()->get();
        $instructors = $this->decodeImageUrl($instructors);
        return response()->json([
            'status' => 'success',
            'message' => "Instructors for subject {$subject_id} retrieved successfully",
            'data' => $instructors
        ]);
    }
}
