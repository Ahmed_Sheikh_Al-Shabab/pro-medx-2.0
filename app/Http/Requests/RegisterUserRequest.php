<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class RegisterUserRequest extends FormRequest
{

    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:120',
            'email' => 'required|email:rfc,dns|unique:users',
            'phone_number' => 'required|max:25|string',
            'password' => ['required', 'min:6', 'regex:/^.*(?=.{2,})(?=.*[a-zA-Z])(?=.*[0-9])/', 'confirmed'],
            'device_uid' => 'required'
        ];
    }
}
