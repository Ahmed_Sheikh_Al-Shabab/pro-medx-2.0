<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterUserRequest;
use App\Models\Feedback;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{

    public function register(RegisterUserRequest $request){
        $validatedUser = $request->validated();
        $user = User::create($validatedUser);
        $token = $user->createToken('pro-medx')->plainTextToken;
        $user->wallet = 0;

        return response()->json([
            'status' => 'success',
            'message' => 'تم التسجيل بنجاح',
            'data' => [
                'user' => $user,
                'token' => $token
            ]
        ], 200);
    }

    public function login(Request $request){

        $request->validate([
            'email' => 'bail|required|email:rfc,dns',
            'password' => 'required|string',
            'device_uid' => 'required|string'
        ]);

        $user = User::where('email', $request->email)->first();
        if (! $user || ! Hash::check($request->password, $user->password)) {
            return response()->json([
                'status' => 'failed',
                'message' => 'معلومات تسجيل الدخول غير صحيحة الرجاء التاكد من صحة البريد الإلكتروني وكلمة المرور',
                'data' => []
            ], 400);
        }

        if ($user->device_uid !== $request->device_uid) {
            return response()->json([
                'status' => 'failed',
                'message' => 'عذرا لا يمكنك تسجيل الدخول من جهاز آخر',
                'data' => []
            ], 400);
        }
        $token = $user->createToken('pro-medx')->plainTextToken;

        return response()->json([
            'status' => 'success',
            'message' => 'تم تسجيل الدخول بنجاح',
            'data' => [
                'user' => $user,
                'token' => $token
            ]
        ], 200);
    }

    public function logout(Request $request){
        $request->user()->tokens()->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'تم تسجيل الخروج بنجاح',
            'data' => []
        ], 200);
    }
}
