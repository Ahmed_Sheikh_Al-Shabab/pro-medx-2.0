<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\ImageTrait;

class Subject extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'image'];

    public function university(){
        return $this->belongsTo(University::class);
    }

    public function instructors(){
        return $this->belongsToMany(Instructor::class, 'subject_instructors')->using(SubjectInstructor::class)->withPivot('id');
    }
}
