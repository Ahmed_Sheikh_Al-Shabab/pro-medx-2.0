<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\ImageTrait;

class Chapter extends Model
{
    use HasFactory;
    protected $fillable = ['title'];

    public function section(){
        return $this->belongsTo(Section::class);
    }

    public function videos(){
        return $this->hasMany(Video::class);
    }
}
