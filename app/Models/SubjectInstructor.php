<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class SubjectInstructor extends Pivot
{
    use HasFactory;

    public $incrementing = true;
    protected $table = 'subject_instructors';

    public function sections(){
        return $this->hasMany(Section::class, 'subject_instructor_id');
    }
}
