<?php

namespace App\Http\Controllers;

use App\Models\University;
use Illuminate\Http\Request;
use App\Http\Traits\ImageTrait;

class UniversityController extends Controller
{
    use ImageTrait;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $universities = University::all();
        $universities = $this->decodeImageUrl($universities);
        return response()->json([
            'status' => 'success',
            'message' => 'Universities retrieved successfully',
            'data' => $universities
        ]);
    }
}
