<?php

namespace App\Http\Controllers;

use App\Models\Qrcode;
use App\Models\User;
use Illuminate\Http\Request;

class UserQrcodeController extends Controller
{
    public function chargeAccount(Request $request){
        $request->validate([
            'code' => 'required|string',
        ]);
        $qrcode = Qrcode::where('code', $request->code)->firstOrFail();
        if($qrcode->is_used){
            return response()->json([
                'status' => 'failed',
                'message' => 'هذا الكود قد تم استخدامه مسبقا',
                'data' => []
            ], 400);
        }
        $user = $request->user();
        $updated_user = $this->updateUserWallet($user, $qrcode);
        $qrcode->is_used = 1; $qrcode->save();
        return response()->json([
            'status' => 'success',
            'message' => 'تم شحن الحساب بنجاح',
            'data' => $updated_user
        ]);
    }

    public function updateUserWallet(User $user, Qrcode $qrcode){
        $current_wallet = $user->wallet;
        $user->wallet = $current_wallet + $qrcode->cash;
        $user->save();
        return $user;
    }
}
