<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $request->validate(['feedback' => 'required|string']);

        $feedback = new Feedback(['feedback' => $request->feedback]);

        $user = User::find($request->user()->id);
        $user->feedbacks()->save($feedback);
        return response()->json([
            'status' => 'success',
            'message' => 'شكرا لك على مساهمتك في تحسين التطبيق',
            'data' => []
        ], 200);
    }
}
