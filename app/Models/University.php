<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\ImageTrait;

class University extends Model
{
    use HasFactory;
    
    protected $fillable = ['faculty', 'city', 'image'];

    public function subjects(){
        return $this->hasMany(Subject::class);
    }
}
