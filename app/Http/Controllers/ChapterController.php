<?php

namespace App\Http\Controllers;

use App\Models\Section;
use Illuminate\Http\Request;
use App\Http\Traits\ImageTrait;

class ChapterController extends Controller
{
    use ImageTrait;
    
    public function getChapter(Request $request, $section_id){
        $section = Section::findOrFail($section_id);
        $chapters = $section->chapters()->get();
        $chapters = $this->decodeImageUrl($chapters);
        return response()->json([
            'status' => 'success',
            'message' => "Chapters for section {$section_id} retrieved successfully",
            'data' => $chapters
        ]);
    }
}
