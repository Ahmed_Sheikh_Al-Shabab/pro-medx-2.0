<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\ImageTrait;

class Instructor extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'image'];

    public function subjects(){
        return $this->belongsToMany(Subject::class, 'subject_instructors')->using(SubjectInstructor::class);
    }
}
