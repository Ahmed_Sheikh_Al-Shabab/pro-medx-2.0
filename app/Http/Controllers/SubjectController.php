<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Traits\ImageTrait;

class SubjectController extends Controller
{
    use ImageTrait;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $university_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, $university_id)
    {
        University::findOrFail($university_id);
        $subjects = Subject::where('university_id', $university_id)->get();
        $subjects = $this->decodeImageUrl($subjects);
        return response()->json([
            'status' => 'success',
            'message' => "Subjects for university {$university_id} retrieved successfully",
            'data' => $subjects
        ]);
    }
}
