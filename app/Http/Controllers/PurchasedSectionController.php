<?php

namespace App\Http\Controllers;

use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PurchasedSectionController extends Controller
{
    public function purchaseSection(Request $request, $section_id){
        $user = $request->user();
        $section = Section::findOrFail($section_id);
        $current_purchased_section = $user->sections()->get();

        if($current_purchased_section->contains('id', $section->id)){
            return response()->json([
                'status' => 'failed',
                'message' => 'هذا القسم تم شراءه مسبقا',
                'data' => []
            ], 400);
        }
        if($user->wallet < $section->price){
            return response()->json([
                'status' => 'failed',
                'message' => 'ليس لديك رصيد كافي لشراء القسم، رجاء قم بشحن رصيدك أولا',
                'data' => []
            ], 400);
        }
        $user->sections()->attach($section->id, ['price' => $section->price]);
        $user->wallet = $user->wallet - $section->price;
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'لقد تم شراء القسم بنجاح يمكنك تصفح محتواه',
            'data' => $user
        ], 200);
    }
}
