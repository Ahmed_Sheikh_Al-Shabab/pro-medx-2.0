<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\ImageTrait;

class Section extends Model
{
    use HasFactory;

   protected $fillable = ['title', 'price', 'details', 'image', 'subject_instructor_id'];

    public function subject_instructor(){
        return $this->belongsTo(SubjectInstructor::class);
    }

    public function chapters(){
        return $this->hasMany(Chapter::class);
    }

    public function users(){
        return $this->belongsToMany(User::class, 'purchased_sections')->using(PurchasedSection::class)->withPivot('price')->withTimestamps();
    }

}
