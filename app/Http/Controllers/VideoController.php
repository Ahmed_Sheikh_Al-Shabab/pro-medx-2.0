<?php

namespace App\Http\Controllers;

use App\Models\Chapter;
use Illuminate\Http\Request;
use App\Http\Traits\ImageTrait;

class VideoController extends Controller
{
    use ImageTrait;
    public function getVideo(Request $request, $chapter_id){
        
        $chapter = Chapter::findOrFail($chapter_id);
        $data = $chapter->videos()->get();
        $data = $this->decodeImageUrl($data);
        return response()->json([
            'status' => 'success',
            'message' => "videos with url for chapter {$chapter_id} retrieved successfully",
            'data' => $data
        ], 200);
    }
}
