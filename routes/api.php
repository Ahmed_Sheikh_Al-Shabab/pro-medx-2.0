<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChapterController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\InstructorController;
use App\Http\Controllers\PurchasedSectionController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\UniversityController;
use App\Http\Controllers\UserQrcodeController;
use App\Http\Controllers\VideoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('auth')->group(function (){
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::middleware(['auth:sanctum'])->post('/logout', [AuthController::class, 'logout'])->name('logout');
});

Route::group([
    'prefix' => 'user',
    'middleware' => ['auth:sanctum'],
], function (){
    Route::post('/feedback', FeedbackController::class);
    Route::post('/charge', [UserQrcodeController::class, 'chargeAccount'])->name('charge');
    Route::post('/purchase/{section_id}', [PurchasedSectionController::class, 'purchaseSection'])->name('purchase_section');

    Route::get('/university', UniversityController::class);
    Route::get('/{university_id}/subject', SubjectController::class);
    Route::get('/{subject_id}/instructor', [InstructorController::Class, 'getInstructor']);
    Route::get('/{subject_instructor_id}/section', [SectionController::class, 'getSection']);
    Route::get('/{section_id}/chapter', [ChapterController::class, 'getChapter']);
    Route::get('/{chapter_id}/video', [VideoController::class, 'getVideo']);
});

Route::get('artisan1',function(){
    $output = [];
    \Artisan::call('config:clear', $output);
    dd($output);
});

Route::get('artisan2',function(){
    $output = [];
    \Artisan::call('cache:clear', $output);
    dd($output);
});

Route::get('artisan3',function(){
    $output = [];
    \Artisan::call('config:cache', $output);
    dd($output);
});
